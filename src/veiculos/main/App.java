package veiculos.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import veiculos.entidades.Caminhao;
import veiculos.entidades.Carro;
import veiculos.entidades.Veiculo;

public class App {
	public static void Menu() {
		System.out.println("***************** Cadastro de Veiculos *****************");
		System.out.println("Escolha uma opção: ");
		System.out.println("1. Cadastrar carro ");
		System.out.println("2. Cadastrar caminhao ");
		System.out.println("3. Ver carros cadastrados ");
		System.out.println("4. Ver caminhões cadastrados ");
		System.out.println("0. Sair ");
	}
	
	public static Veiculo cadastrarVeiculo(Veiculo veiculo, Scanner ler, String propriedades) {
		System.out.print("\n Ano: ");
		propriedades = ler.nextLine();
		veiculo.setAno(propriedades);
		
		System.out.print("\n Modelo: ");
		propriedades = ler.nextLine();
		veiculo.setModelo(propriedades);
		
		System.out.print("\n Placa: ");
		propriedades = ler.nextLine();
		veiculo.setPlaca(propriedades);
		
		System.out.print("\n Fabricante: ");
		propriedades = ler.nextLine();
		veiculo.setFabricante(propriedades);
		
		System.out.print("\n Cor: ");
		propriedades = ler.nextLine();
		veiculo.setCor(propriedades);
		
		System.out.print("\n Chassi: ");
		propriedades = ler.nextLine();
		veiculo.setChassi(propriedades);
		
		return veiculo;
	}
	
	public static Carro cadastrarCarro (Carro carro, Scanner ler) {
		String propriedades = "";
		int nroAcessorios;
		
		carro = (Carro) cadastrarVeiculo(carro, ler, propriedades);
		
		System.out.print("\n Número de acessórios: ");
		nroAcessorios = ler.nextInt();
		
		if(nroAcessorios != 0) {
			carro.setAcessorios(nroAcessorios);
			for (int i=0; i<carro.getAcessorios().length; i++) {
				System.out.print("\n Digite o nome do acessório " + i+1 + ": ");
				propriedades = ler.nextLine();
				carro.setVetorAcessorios(i, propriedades);
			}
		}
		
		return carro;
		
	}
	
	public static Caminhao cadastrarCaminhao(Caminhao caminhao, Scanner ler) {
		String propriedades = "";
		List<Veiculo> carretas = null;
		int nroCarretas;
		
		caminhao = (Caminhao) cadastrarVeiculo(caminhao, ler, propriedades);
		
		System.out.print("\n Digite o número de carretas que o caminhão tem: ");
		nroCarretas = ler.nextInt();
		
		if (nroCarretas != 0) {
			Veiculo veiculo = null;
			for(int i=0; i<nroCarretas; i++) {
				System.out.print("\n Digite as propriedades da carreta " + i+1 + ": ");
				veiculo = cadastrarVeiculo(veiculo, ler, propriedades);
				carretas.add(veiculo);
			}
			caminhao.setCarretas(carretas);
		}
		
		return caminhao;
	}
	
	public static void main(String[] args) {
		int menu = -1;
		Scanner ler = new Scanner(System.in);
		List<Carro> listaCarros= new ArrayList();
		List<Caminhao> listaCaminhoes = new ArrayList();
		
		while (menu != 0) {
			Menu();
			menu = ler.nextInt();
			Carro carro = new Carro();
			Caminhao caminhao = null;
			
			switch(menu) {
			
			case 0:
				break;
			
			case 1:
				//carro = new Carro();
				System.out.println("Digite as propriedades do veiculo: ");
				carro = cadastrarCarro(carro, ler);
				listaCarros.add(carro);
			break;
			
			case 2:
				caminhao = new Caminhao();
				System.out.println("Digite as propriedades do veiculo: ");
				caminhao = cadastrarCaminhao(caminhao, ler);
				listaCaminhoes.add(caminhao);
			break;
			
			case 3:
				for (Carro lista: listaCarros) {
					lista.toString();
				}
			break;
			
			case 4:
				caminhao.toString();
			break;
			
			default:
				System.out.println("Opção inexistente!");
			break;
			
			}
			
		}
		System.out.println("Fim");
	}

}
