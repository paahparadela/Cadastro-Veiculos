package veiculos.entidades;

import java.util.List;

public class Caminhao extends Veiculo {
	List<Veiculo> carretas;

	public List<Veiculo> getCarretas() {
		return carretas;
	}

	public void setCarretas(List<Veiculo> carretas) {
		this.carretas = carretas;
	}
	
	@Override
	public String toString() {
		return "Caminhão ******" + "\n Ano: " +ano+ "\n Modelo: " +modelo+ "\n Placa: "
				+placa+ "\n Fabricante: " +fabricante+ "\n Cor: " +cor+ "\n Chassi: " +chassi;
		
	}
}
