package veiculos.entidades;

public class Carro extends Veiculo {
	protected String [] acessorios;

	public String[] getAcessorios() {
		return acessorios;
	}

	public void setAcessorios(int acessorios) {
		this.acessorios = new String[acessorios];
	}
	
	public void setVetorAcessorios(int index, String acessorio) {
		acessorios[index] = acessorio;
	}
	
	public String toString() {
		return "Carro ******" + "\n Ano: " +ano+ "\n Modelo: " +modelo+ "\n Placa: "
				+placa+ "\n Fabricante: " +fabricante+ "\n Cor: " +cor+ "\n Chassi: " +chassi;
	}
}
